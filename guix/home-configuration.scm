;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu services)
             (guix gexp)
             (gnu home services shells))

(home-environment
  ;; Below is the list of packages that will show up in your
  ;; Home profile, under ~/.guix-home/profile.
  (packages (specifications->packages (list "emacs"
                                            "glibc-locales"
                                            "emacs-geiser-guile"
                                            "emacs-geiser"
                                            "guile"
                                            "neovim"
                                            "htop"
                                            "neofetch"
                                            "tree"
                                            "git"
                                            "btop"
                                            "nmap"
                                            "dex"
                                            "f3"
                                            "awscli@2"
                                            "lolcat")))

  ;; Below is the list of Home services.  To search for available
  ;; services, run 'guix home search KEYWORD' in a terminal.
  (services
   (list (service home-bash-service-type
                  (home-bash-configuration
                   (aliases '(("ls" . "ls --color=auto")))
                   (bashrc (list (local-file "./.bashrc" "bashrc")))
                   (bash-logout (list (local-file "./.bash_logout"
                                                  "bash_logout"))))))))
