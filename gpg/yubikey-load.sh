#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

PUBLIC_GPG_KEY_FILE="public-v3.gpg.asc"

case $(uname -s) in
  Linux)
    if [[ $(systemctl is-active pcscd) == active ]]; then
      systemctl enable --now pcscd
    else
      systemctl restart pcscd
    fi
    ;;
esac

[[ -f ${PUBLIC_GPG_KEY_FILE} ]] \
  && gpg --quiet --import ${PUBLIC_GPG_KEY_FILE} \
  && gpg-connect-agent --quiet "scd serialno" "learn --force" /bye
