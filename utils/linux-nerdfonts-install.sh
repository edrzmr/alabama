#!/bin/bash

# https://www.nerdfonts.com/font-downloads
# https://github.com/ryanoasis/nerd-fonts/releases

fonts=(
  0xProto
  "3270"
  Agave
  AnonymousPro
  Arimo
  AurulentSansMono
  BigBlueTerminal
  BitstreamVeraSansMono
  CascadiaCode
  CascadiaMono
  CodeNewRoman
  ComicShannsMono
  CommitMono
  Cousine
  D2Coding
  DaddyTimeMono
  DejaVuSansMono
  DroidSansMono
  EnvyCodeR
  FantasqueSansMono
  FiraCode
  FiraMono
  GeistMono
  Go-Mono
  Gohu
  Hack
  Hasklig
  HeavyData
  Hermit
  iA-Writer
  IBMPlexMono
  InconsolataGo
  InconsolataLGC
  Inconsolata
  InconsolataGo
  InconsolataLGC
  IntelOneMono
  Iosevka
  IosevkaTerm
  IosevkaTermSlab
  JetBrainsMono
  Lekton
  LiberationMono
  Lilex
  MartianMono
  Meslo
  Monaspace
  Monofur
  Monoid
  Mononoki
  MPlus
  NerdFontsSymbolsOnly
  Noto
  OpenDyslexic
  Overpass
  ProFont
  ProggyClean
  Recursive
  RobotoMono
  ShareTechMono
  SourceCodePro
  SpaceMono
  Terminus
  Tinos
  Ubuntu
  UbuntuMono
  UbuntuSans
  VictorMono
  ZedMono
)

declare -A urls
for font in ${fonts[@]}; do
  urls[$font]="https://github.com/ryanoasis/nerd-fonts/releases/latest/download/$font.tar.xz"
done

declare -A installDirs
for font in ${fonts[@]}; do
  installDirs[$font]="${HOME}/.local/share/fonts/$font"
done

# begin, prepare environment
tmpDir="/tmp/nerdfonts-installer"
rm --recursive --force --verbose ${tmpDir}
mkdir --parents ${tmpDir}
downloadDir=$(mktemp --directory --tmpdir=${tmpDir} -t download-XXXXXXXXXX)
mkdir --parents ${downloadDir}
for dir in ${installDirs[@]}; do # ensure that does not exists
  rm --recursive --force $dir
  mkdir --parents $dir
done
fc-cache --force
# end, prepare environment

declare -A downloadFiles
for font in ${fonts[@]}; do
  downloadFiles[$font]="${downloadDir}/$font.tar.xz"
done

# for key in ${fonts[@]}; do echo "=> font: $key, url: ${urls[$key]}, installDir: ${installDirs[$key]}"; done
echo "=== download ===================="
pushd ${downloadDir} > /dev/null
echo ${urls[@]} | xargs --verbose --max-procs=10 --max-args=1 wget --quiet
popd > /dev/null


echo "=== extract ===================="
for key in ${fonts[@]}; do
  cmd="tar --xz --extract --file=${downloadFiles[$key]} --directory ${installDirs[$key]}"
  echo ${cmd}
  eval $cmd
done
fc-cache --force

# cleanup
rm --recursive --force ${tmpDir}
