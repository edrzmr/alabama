pkgs: {
  enable = true;
  autosuggestion = {
    enable = true;
  };
  enableCompletion = true;
  enableVteIntegration = false;
  syntaxHighlighting = {
    enable = true;
  };
  history = {
    save = 100000;
    size = 100000;
    extended = false;
    ignoreDups = true;
    ignoreSpace = true;
    share = false;
    expireDuplicatesFirst = true;
  };
  shellAliases = {
    ls = "ls --color=auto --group-directories-first --classify";
    ll = "ls --color=auto --group-directories-first --classify -t -r";
    grep = "grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn}";
    zsh-reload = "reset && unset __HM_ZSH_SESS_VARS_SOURCED && source ~/.zshenv && source ~/.zshrc";
  };
  sessionVariables = {
    _LOCAL_ZSH_RC_LOCAL = "$HOME/.zshrc.local";
    _LOCAL_SDKMAN_DIR = "$HOME/.local/opt/sdkman/current";
    _LOCAL_AWS_CLI_DIR = "$HOME/.local/opt/awscli/current";
    _LOCAL_BIN_DIR = "$HOME/.local/bin";
    DOTFILE_ZSH_RC_CHEZMOI = "$HOME/.zshrc.chezmoi";
    EDITOR = "vim";
    SSH_AUTH_SOCK="/run/user/$(id -u)/gnupg/S.gpg-agent.ssh";
  };

  plugins = [
    {
      name = "zsh-history-substring-search";
      src = pkgs.fetchFromGitHub { owner = "zsh-users"; repo = "zsh-history-substring-search"; rev = "v1.0.2"; sha256 = "Ptxik1r6anlP7QTqsN1S2Tli5lyRibkgGlVlwWZRG3k="; };
    }
  ];

  oh-my-zsh = {
    enable = true;
    plugins = [ "colored-man-pages" "command-not-found" ];
  };

  initExtraFirst = ''
    fpath+="$HOME/.zsh/completions"
    fpath+="/usr/share/zsh/site-functions"
  '';

  profileExtra = ''
    #################
    # configure VTE #
    #################
    [[ -n $TILIX_ID ]] || [[ $VTE_VERSION ]] && [[ -f /etc/profile.d/vte.sh ]] && source /etc/profile.d/vte.sh

    ##################
    # configure guix #
    ##################
    [[ -d $HOME/.config/guix/current/bin ]] && export PATH="$HOME/.config/current/bin:$PATH"
    [[ -f /etc/profile.d/guix.sh ]] && source /etc/profile.d/guix.sh
  '';

  initExtra = ''
    ########
    # Brew #
    ########
    [[ -n $_LOCAL_BREW_DIR ]] && BREW="$_LOCAL_BREW_DIR/bin/brew"
    [[ -x $BREW ]] && eval "$($BREW shellenv)"
    [[ -d $HOMEBREW_PREFIX ]] && fpath+="$HOMEBREW_PREFIX/share/zsh/site-functions"

    ####################
    # show pretty path #
    ####################
    function pretty-path() { echo $PATH | tr ":" "\n" }

    #######################################
    # reload/refresh gpg from key/yubikey #
    #######################################
    function gpg-key-reload() {
      [[ $(uname -s) == Linux ]] && \
        [[ $(gpg-connect-agent "scd serialno" /bye | tail -n1) != OK ]] && \
        sudo systemctl restart pcscd && \
        gpg-connect-agent "learn --force" /bye
    }

    ##########
    # sdkman #
    ##########
    [[ -n $_LOCAL_SDKMAN_DIR ]] && export SDKMAN_DIR=$_LOCAL_SDKMAN_DIR
    [[ -r $SDKMAN_DIR/bin/sdkman-init.sh ]] && source $SDKMAN_DIR/bin/sdkman-init.sh
    # [[ -r $SDKMAN_DIR/bin/sdkman-init.sh ]] && echo ">>> sdkman => $(sdk version | grep ^script), $(sdk version | grep ^native)"

    #######
    # aws #
    #######
    [[ -n $_LOCAL_AWS_CLI_DIR ]] && export AWS_CLI_INSTALL_DIR=$_LOCAL_AWS_CLI_DIR
    [[ ! -d $AWS_CLI_INSTALL_DIR ]] && mkdir -p $AWS_CLI_INSTALL_DIR
    [[ -x $_LOCAL_BIN_DIR/aws_completer ]] && complete -C aws_completer aws
    [[ -x $_LOCAL_BIN_DIR/aws ]] && echo ">>> awscli => $(aws --version)"


    ######
    # cs #
    ######
    [[ -d $HOME/.local/share/coursier/bin ]] && export PATH="$HOME/.local/share/coursier/bin:$PATH"

    ####################
    # load local zshrc #
    ####################
    [[ -r $DOTFILE_ZSH_RC_CHEZMOI ]] && source $DOTFILE_ZSH_RC_CHEZMOI
    [[ -r $_LOCAL_ZSH_RC_LOCAL ]] && echo ">>> loading => $_LOCAL_ZSH_RC_LOCAL..."
    [[ -r $_LOCAL_ZSH_RC_LOCAL ]] && source $_LOCAL_ZSH_RC_LOCAL
  '';
}
